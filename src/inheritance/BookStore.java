//Mate Barabas 1834578
package inheritance;

public class BookStore
{

	public static void main(String[] args)
	{
		Book[] b = new Book[5];
		
		b[0] = new Book("Lord of The Rings", "Tolkien");
		b[1] = new ElectronicBook("Java for dummies", "Dan Pomerantz", 655);
		b[2] = new Book("The art of code", "A. Lovelace");
		b[3] = new ElectronicBook("SQL for dummies", "Andrew Bodzay", 720);
		b[4] = new ElectronicBook("JavaScript for dummies", "Jaya", 440);
		
		for (Book a : b)
		{
			System.out.println(a.toString());
		}
	}

}
