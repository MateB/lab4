//Mate Barabas 1834578
package inheritance;

public class ElectronicBook extends Book
{
	public int numberBytes;
	
	public ElectronicBook(String title, String author, int numberBytes)
	{
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public String toString()
	{
		String fromBase = super.toString();
		return fromBase + ", Number of Bytes: " + this.numberBytes;
	}
}
