//Mate Barabas 1834578
package geometry;

public class Circle implements Shape 
{
	private double area;
	private double perimeter;
	private double radius;
	
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	public double getRadius()
	{
		return this.radius;
	}
	
	public double getArea()
	{	
		this.area = Math.PI*(Math.pow(this.radius, 2));
		return this.area;
	}
	
	public double getPerimeter()
	{	
		this.perimeter = 2 * Math.PI * this.radius;
		return this.perimeter;
	}
	//adding this extra method to make the printing look nice
	public String getName()
	{
		return "Circle";
	}
}
