//Mate Barabas 1834578
package geometry;

public class Rectangle implements Shape 
{
	private double width;
	private double length;
	private double area;
	private double perimeter;
	
	public Rectangle(double width, double length)
	{
		this.width = width;
		this.length = length;
	}
	
	public double getWidth()
	{
		return this.width;
	}
	
	public double getLength()
	{
		return this.length;
	}
	
	public double getArea()
	{
		this.area = this.width * this.length;
		return this.area;
	}
	
	public double getPerimeter()
	{
		this.perimeter = (this.width * 2) + (this.length * 2);
		return this.perimeter;
	}
	//adding this extra method to make the printing look nice
	public String getName()
	{
		return "Rectangle";
	}
}
