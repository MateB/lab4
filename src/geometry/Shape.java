//Mate Barabas 1834578
package geometry;

public interface Shape 
{
	double getArea();
	double getPerimeter();
	//adding this to make printing specify what shape it is
	String getName();
}
