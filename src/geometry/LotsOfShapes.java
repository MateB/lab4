//Mate Barabas 1834578
package geometry;

public class LotsOfShapes 
{

	public static void main(String[] args) 
	{
		Shape[] s = new Shape[5];
		
		s[0] = new Rectangle(2,2);
		s[1] = new Rectangle(2,4);
		s[2] = new Circle(5);
		s[3] = new Circle(8);
		s[4] = new Square(2);
		
		for (Shape a : s)
		{
			System.out.println("Shape: " + a.getName() + ", Area: " + a.getArea() + ", Perimeter: " + a.getPerimeter());
		}
	}

}
