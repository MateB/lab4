//Mate Barabas 1834578
package geometry;

public class Square extends Rectangle
{
	public Square(double sideLength)
	{
		super(sideLength,sideLength);
	}
	//adding this extra method to make the printing look nice
	public String getName()
	{
		return "Square";
	}
}

